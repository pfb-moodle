<?php // $Id$
/**
 * Export quiz questions into the given category
 *
 * @author Martin Dougiamas, Howard Miller, and many others.
 *         {@link http://moodle.org}
 * @license http://www.gnu.org/copyleft/gpl.html GNU Public License
 * @package questionbank
 * @subpackage importexport
 */

    require_once("../config.php");
    require_once( "editlib.php" );

    list($thispageurl, $courseid, $cmid, $cm, $module, $pagevars) = question_edit_setup();

    $cattofile = optional_param('cattofile',0, PARAM_BOOL);
    
    $exportfilename = optional_param('exportfilename','',PARAM_FILE );
    $format = optional_param('format','', PARAM_FILE );


    // get display strings
    $txt = new object;
    $txt->category = get_string('category','quiz');
    $txt->download = get_string('download','quiz');
    $txt->downloadextra = get_string('downloadextra','quiz');
    $txt->exporterror = get_string('exporterror','quiz');
    $txt->exportname = get_string('exportname','quiz');
    $txt->exportquestions = get_string('exportquestions', 'quiz');
    $txt->fileformat = get_string('fileformat','quiz');
    $txt->exportcategory = get_string('exportcategory','quiz');
    $txt->modulename = get_string('modulename','quiz');
    $txt->modulenameplural = get_string('modulenameplural','quiz');
    $txt->tofile = get_string('tofile','quiz');


    if (!$course = get_record("course", "id", $courseid)) {
        error("Course does not exist!");
    }


    if (!$category = get_record("question_categories", "id", $pagevars['cat'])) {   
        $category = get_default_question_category($courseid); 
    }
    
    if (!$categorycourse = get_record("course", "id", $category->course)) {
        print_error('nocategory','quiz');
    }


    // check role capability
    $context = get_context_instance(CONTEXT_COURSE, $course->id);
    require_capability('moodle/question:export', $context);

    // ensure the files area exists for this course
    make_upload_directory( "$course->id" );

    // check category is valid
    if (!empty($pagevars['cat'])) {
        $validcats = question_category_options( $course->id, true, false );
        if (!array_key_exists( $pagevars['cat'], $validcats)) {
            print_error( 'invalidcategory','quiz' );
        }
    }

    /// Header
    if ($cm!==null) {
        $strupdatemodule = has_capability('moodle/course:manageactivities', get_context_instance(CONTEXT_COURSE, $course->id))
            ? update_module_button($cm->id, $course->id, get_string('modulename', $cm->modname))
            : "";
        $navlinks = array();
        $navlinks[] = array('name' => get_string('modulenameplural', $cm->modname), 'link' => "$CFG->wwwroot/mod/{$cm->modname}/index.php?id=$course->id", 'type' => 'activity');
        $navlinks[] = array('name' => format_string($module->name), 'link' => "$CFG->wwwroot/mod/{$cm->modname}/view.php?cmid={$cm->id}", 'type' => 'title');
        $navlinks[] = array('name' => $txt->exportquestions, 'link' => '', 'type' => 'title');
        $navigation = build_navigation($navlinks);
        print_header_simple($txt->exportquestions, '', $navigation, "", "", true, $strupdatemodule);

        $currenttab = 'edit';
        $mode = 'export';
        ${$cm->modname} = $module;
        include($CFG->dirroot."/mod/$cm->modname/tabs.php");
    } else {
        // Print basic page layout.
        $navlinks = array();
        $navlinks[] = array('name' => $txt->exportquestions, 'link' => '', 'type' => 'title');
        $navigation = build_navigation($navlinks);
           
        print_header_simple($txt->exportquestions, '', $navigation);
        // print tabs
        $currenttab = 'export';
        include('tabs.php');
    }

    if (!empty($format)) {   /// Filename

        if (!confirm_sesskey()) {
            print_error( 'sesskey' );
        }

        if (! is_readable("format/$format/format.php")) {
            error( "Format not known ($format)" );  }

        // load parent class for import/export
        require("format.php");

        // and then the class for the selected format
        require("format/$format/format.php");

        $classname = "qformat_$format";
        $qformat = new $classname();

        $qformat->setCategory( $category );
        $qformat->setCourse( $course );
        $qformat->setFilename( $exportfilename );
        $qformat->setCattofile( $cattofile );

        if (! $qformat->exportpreprocess()) {   // Do anything before that we need to
            error( $txt->exporterror, $thispageurl->out());
        }

        if (! $qformat->exportprocess()) {         // Process the export data
            error( $txt->exporterror, $thispageurl->out());
        }

        if (! $qformat->exportpostprocess()) {                    // In case anything needs to be done after
            error( $txt->exporterror, $thispageurl->out());
        }
        echo "<hr />";

        // link to download the finished file
        $file_ext = $qformat->export_file_extension();
        if ($CFG->slasharguments) {
          $efile = "{$CFG->wwwroot}/file.php/".$qformat->question_get_export_dir()."/$exportfilename".$file_ext."?forcedownload=1";
        }
        else {
          $efile = "{$CFG->wwwroot}/file.php?file=/".$qformat->question_get_export_dir()."/$exportfilename".$file_ext."&forcedownload=1";
        }
        echo "<p><div class=\"boxaligncenter\"><a href=\"$efile\">$txt->download</a></div></p>";
        echo "<p><div class=\"boxaligncenter\"><font size=\"-1\">$txt->downloadextra</font></div></p>";

        print_continue("edit.php?".$thispageurl->get_query_string());
        print_footer($course);
        exit;
    }

    /// Display upload form

    // get valid formats to generate dropdown list
    $fileformatnames = get_import_export_formats( 'export' );

    // get filename
    if (empty($exportfilename)) {
        $exportfilename = default_export_filename($course, $category);
    }

    print_heading_with_help($txt->exportquestions, 'export', 'quiz');
    print_simple_box_start('center');
?>

    <form enctype="multipart/form-data" method="post" action="export.php">
        <fieldset class="invisiblefieldset" style="display: block;">
            <input type="hidden" name="sesskey" value="<?php echo sesskey(); ?>" />
            <?php echo $thispageurl->hidden_params_out(array(), 3); ?>
            <table cellpadding="5">
                <tr>
                    <td align="right"><?php echo $txt->category; ?>:</td>
                    <td>
                        <?php
                        question_category_select_menu($course->id, true, false, $category->id);
                        echo $txt->tofile; ?>
                        <input name="cattofile" type="checkbox" />
                        <?php helpbutton('exportcategory', $txt->exportcategory, 'quiz'); ?>
                    </td>
                </tr>
                <tr>
                    <td align="right"><?php echo $txt->fileformat; ?>:</td>
                    <td>
                        <?php choose_from_menu($fileformatnames, 'format', 'gift', '');
                        helpbutton('export', $txt->exportquestions, 'quiz'); ?>
                    </td>
                </tr>
                <tr>
                    <td align="right"><?php echo $txt->exportname; ?>:</td>
                    <td>
                        <input type="text" size="40" name="exportfilename" value="<?php echo $exportfilename; ?>" />
                    </td>
                </tr>
                <tr>
                    <td align="center" >
                        <input type="submit" name="save" value="<?php echo $txt->exportquestions; ?>" />
                    </td>
                    <td>&nbsp;</td>
                </tr>
            </table>
        </fieldset>
    </form>
    <?php

    print_simple_box_end();
    print_footer($course);
?>

