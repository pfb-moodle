<?php // $Id$

////////////////////////////////////////////////////////////////////////////////
//  Code fragment to define the version of quiz
//  This fragment is called by moodle_needs_upgrading() and /admin/index.php
////////////////////////////////////////////////////////////////////////////////

$module->version  = 2007070200;   // The (date) version of this module
$module->requires = 2007062401;   // Requires this Moodle version
$module->cron     = 0;            // How often should cron check this module (seconds)?

?>
