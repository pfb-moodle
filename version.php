<?php

// MOODLE VERSION INFORMATION

// This file defines the current version of the core Moodle code being used.
// This is compared against the values stored in the database to determine
// whether upgrades should be performed (see lib/db/*.php)

   $version = 2007070603;  // YYYYMMDD = date
                           //       XY = increments within a single day

   $release = '1.9 dev';    // Human-friendly version name

?>
