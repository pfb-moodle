<?PHP // $Id$
      // note.php 
$string['notes'] = 'Notes';
$string['sitenotes'] = 'Site notes';
$string['coursenotes'] = 'Course notes';
$string['personalnotes'] = 'Personal notes';
$string['created'] = 'created';
$string['rating'] = 'Rating';
$string['nonotes'] = 'There are no notes.';
$string['notesnotvisible'] = 'You are not allowed to view the notes.';
$string['addnewnote'] = 'Add a new note';
$string['groupaddnewnote'] = 'Add a new note for all';
$string['deleteconfirm'] = 'Delete this note?';
$string['content'] = 'Note content';
$string['nocontent'] = 'Note content can not be empty';
$string['nouser'] = 'You must select a user';
$string['rating'] = 'Rating';
$string['low'] = 'low';
$string['belownormal'] = 'below normal';
$string['normal'] = 'normal';
$string['abovenormal'] = 'above normal';
$string['high'] = 'high';
$string['unknown'] = 'unknown';
$string['bynameondate'] = 'by $a->name - $a->date';
$string['publishstate'] = 'Status';
$string['personal'] = 'personal';
$string['course'] = 'course';
$string['site'] = 'site';
?>